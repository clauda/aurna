# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stat do
    candidate nil
    election nil
    votes 1
    percent "MyString"
  end
end
