# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :election do
    year 1
    round 1
  end
end
