# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :total_stat do
    position "MyString"
    state "MyString"
    election nil
    total 1
  end
end
