# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :vote do
    candidate nil
    is_null false
    is_white false
    election nil
    user nil
  end
end
