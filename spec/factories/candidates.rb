# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :candidate do
    name "MyString"
    ballot_name "MyString"
    position "MyString"
    number 1
    entourage "MyString"
    status "MyString"
    union "MyString"
  end
end
