class AddPositionToVote < ActiveRecord::Migration
  def change
    add_column :votes, :position, :string
  end
end
