class CreateTotalStats < ActiveRecord::Migration
  def change
    create_table :total_stats do |t|
      t.string :position
      t.string :state
      t.references :election, index: true
      t.integer :total
    end
  end
end
