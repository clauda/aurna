class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.references :user, index: true
      t.text :message
      t.string :status

      t.timestamps
    end
  end
end
