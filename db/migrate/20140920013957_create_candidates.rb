class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :ballot_name
      t.string :position
      t.integer :number
      t.string :entourage
      t.string :status
      t.string :union
      t.string :state
    end

    add_index :candidates, :number
  end
end
