class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.boolean :is_null, default: false
      t.boolean :is_white, default: false
      t.references :candidate, index: true
      t.references :election, index: true
      t.references :user, index: true
    end
  end
end
