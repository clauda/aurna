class CreateStats < ActiveRecord::Migration
  def change
    create_table :stats do |t|
      t.references :candidate, index: true
      t.references :election, index: true
      t.integer :votes
      t.string :percent

      t.timestamps
    end
  end
end
