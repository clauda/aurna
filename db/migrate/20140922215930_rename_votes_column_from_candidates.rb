class RenameVotesColumnFromCandidates < ActiveRecord::Migration
  def change
    rename_column :candidates, :votes, :votes_counter
  end
end
