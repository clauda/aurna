class AddElectionToCandidate < ActiveRecord::Migration
  def change
    add_reference :candidates, :election, index: true
  end
end
