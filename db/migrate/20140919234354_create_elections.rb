class CreateElections < ActiveRecord::Migration
  def change
    create_table :elections do |t|
      t.integer :year
      t.integer :round
    end
  end
end
