class AddVotesToCandidates < ActiveRecord::Migration
  def change
    add_column :candidates, :votes, :integer
  end
end
