# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140923023926) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "candidates", force: true do |t|
    t.string  "name"
    t.string  "ballot_name"
    t.string  "position"
    t.integer "number"
    t.string  "entourage"
    t.string  "status"
    t.string  "union"
    t.string  "state"
    t.integer "votes_counter"
    t.integer "election_id"
  end

  add_index "candidates", ["election_id"], name: "index_candidates_on_election_id", using: :btree
  add_index "candidates", ["number"], name: "index_candidates_on_number", using: :btree

  create_table "elections", force: true do |t|
    t.integer "year"
    t.integer "round"
  end

  create_table "identities", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "issues", force: true do |t|
    t.integer  "user_id"
    t.text     "message"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "issues", ["user_id"], name: "index_issues_on_user_id", using: :btree

  create_table "stats", force: true do |t|
    t.integer  "candidate_id"
    t.integer  "election_id"
    t.integer  "votes"
    t.string   "percent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stats", ["candidate_id"], name: "index_stats_on_candidate_id", using: :btree
  add_index "stats", ["election_id"], name: "index_stats_on_election_id", using: :btree

  create_table "total_stats", force: true do |t|
    t.string  "position"
    t.string  "state"
    t.integer "election_id"
    t.integer "total"
  end

  add_index "total_stats", ["election_id"], name: "index_total_stats_on_election_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.boolean "is_null",      default: false
    t.boolean "is_white",     default: false
    t.integer "candidate_id"
    t.integer "election_id"
    t.integer "user_id"
    t.string  "position"
  end

  add_index "votes", ["candidate_id"], name: "index_votes_on_candidate_id", using: :btree
  add_index "votes", ["election_id"], name: "index_votes_on_election_id", using: :btree
  add_index "votes", ["user_id"], name: "index_votes_on_user_id", using: :btree

end
