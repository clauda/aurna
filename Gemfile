source 'https://rubygems.org'
ruby '2.1.3'

gem 'rails', '4.1.6'
gem 'pg'

# Assets
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'

# Caching
gem 'rack-cache'
gem 'actionpack-action_caching'
gem 'redis'
gem 'redis-rails'

# Auth
gem 'devise'
gem 'omniauth'
gem 'omniauth-facebook'

# Index
gem 'elasticsearch-model', git: 'git://github.com/elasticsearch/elasticsearch-rails.git'
gem 'elasticsearch-rails', git: 'git://github.com/elasticsearch/elasticsearch-rails.git'

# Addons
gem 'jbuilder'
gem 'inherited_resources'
gem 'dotenv-rails'
gem 'kaminari'
gem 'sidekiq'

# Production
gem 'unicorn'
gem 'rails_12factor', group: :production
gem 'capistrano', require: false
gem 'capistrano-rails', require: false
gem 'rvm1-capistrano3', require: false
gem 'capistrano-bundler', require: false
gem 'capistrano-sidekiq', group: :development

group :development, :test do
  gem 'rspec-rails', '~> 3.0.0'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'pry-rails'
  gem 'rspec'
  gem 'therubyracer'
  gem 'thin'
end

group :development do
  gem 'spring'
  gem 'better_errors'
  gem 'binding_of_caller'
end

group :test do
  gem 'database_cleaner'
end

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

