module Worker

  class Stats
    include CandidatesHelper

    attr_accessor :current_election

    def initialize current_election = nil
      @current_election = current_election || Election.find_or_create_by(year: SETTINGS[:election][:year], round: SETTINGS[:election][:round])
    end

    def update_votes!
      Candidate.active.where(election: @current_election).each do |candidate|
        VotesWorker.perform_async candidate.id if candidate
      end
    end

    def update_totals!
      @positions = Candidate.active.where(election: @current_election).select(:position).uniq.pluck(:position)
      @states = CandidatesHelper::STATES.keys

      @states.each do |state|
        @positions.each do |position|
          totalstats = TotalStat.find_or_create_by(election: @current_election, position: position, state: state)

          total = Candidate.where(election: @current_election, position: position, state: state).sum(:votes_counter)
          totalstats.update_column :total, total
        end
      end
    end

    def update_stats!
      Candidate.active.where(election: @current_election).each do |candidate|
        StatsWorker.perform_async candidate.id if candidate
      end
    end

  end

end