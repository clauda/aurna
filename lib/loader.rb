# encoding: utf-8
require 'csv'

module Loader
  class Candidates
    LIST = YAML::load_file(File.join(Rails.root,'data','candidates.yml'))

    def initialize
      @current_election ||= Election.find_or_create_by(year: SETTINGS[:election][:year], round: SETTINGS[:election][:round])
    end

    def start! state = nil
      if state
        LIST[state.upcase].each { |file| load(file, state, @current_election.id) }
      else
        LIST.each do |state, files|
          puts "\nCarregando #{state}...'"
          files.each {|file| load(file, state, @current_election.id) }
        end
      end

      puts "\n\nDone!"
    end

    protected

      def load file, state, election_id
        puts "\nReading #{file}:'"
        CSV.foreach(File.join(Rails.root,'data',file), col_sep: ';', encoding: "iso-8859-1", headers: true) do |row|
          puts row[0]

         Candidate.create(
              name: row[0], ballot_name: row[1], position: row[2],
              number: row[3].to_i, entourage: row[4], status: row[5],
              union: row[6], state: state, election_id: election_id)
        end
      end

  end
end