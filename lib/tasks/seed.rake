# encoding: utf-8
namespace :load do

  desc 'Carregar todos os candidatos'
  task :candidates, [:state] => :environment do |t, args|
    loader = Loader::Candidates.new
    loader.start! args.state
  end

end