require 'elasticsearch/rails/tasks/import'

# elasticsearch-rails comes with a bunch of "ready-to-use" import tasks, except that
# 1. they don't depend on the environment task, so you have to call it manually before them
# 2. they are kind of verbose
#
# This file adds two extra tasks to the elasticsearch namespace for convenience
# - elasticsearch:regenerate is equivalent to ':import:all FORCE=y', that is, it rebuilds
#   all indexes and imports content from the database to the running ES instance
# - elasticsearch:update is equivalent to :import:model
# Both tasks deal with the auto-completion field, updating it as needed, and both tasks
# depend on the environment task, so you don't have to call it explicitely

namespace :elasticsearch do

  task regenerate: [:environment] do
    ENV['FORCE'] = 'y'
    Rake::Task['elasticsearch:import:all'].execute
  end

  task update: [:environment] do
    Rake::Task['elasticsearch:import:model'].execute
  end

end