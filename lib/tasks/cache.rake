namespace :cache do

  desc "Clear aaaaall cache"
  task :clear => [:environment] do
    Rails.cache.clear
  end

end