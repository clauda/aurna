# encoding: utf-8
namespace :jobs do

  desc 'Atualiza a contagem dos votos'
  task votes: [:environment] do |t, args|
    worker = Worker::Stats.new
    worker.update_votes!
  end

  desc 'Atualiza a tabela de totais de votos'
  task totals: [:environment] do |t, args|
    worker = Worker::Stats.new
    worker.update_totals!
  end

  desc 'Atualiza a tabela de estatísticas'
  task stats: [:environment] do |t, args|
    worker = Worker::Stats.new
    worker.update_stats!
  end

end