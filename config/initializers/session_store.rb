# Be sure to restart your server when you modify this file.

# Rails.application.config.session_store :cookie_store, key: '_aurna_session'

Aurna::Application.config.session_store :redis_store, redis_server: ENV['REDISCLOUD_URL'], expires_in: 30.minutes
