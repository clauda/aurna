Rails.application.routes.draw do

  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }, skip: [:sessions]

  as :user do
    get 'participar' => "devise/sessions#new", as: :new_user_session
    post 'signin' => 'devise/sessions#create', as: :user_session
    delete 'sair' => 'devise/sessions#destroy', as: :destroy_user_session
  end

  match '/perfil' => 'users#account', via: [:get, :patch], as: :account

  namespace :zone do
    root 'dashboard#index'
    get '/stats(/:state/:position)' => 'dashboard#stats', as: :stats
    post '/search' => "candidates#index"
    resources :elections, only: [:index, :create]
    resources :users, only: [:index, :edit, :update]
    resources :candidates
    resources :issues, only: [:index, :update, :edit, :show]
  end

  resources :issues, only: [:create]

  get '/simulador', to: 'votes#index', as: :simulator
  get '/simulador/resultados', to: 'votes#results', as: :results
  get '/simulador/resultados/imprimir', to: 'votes#print', as: :print
  post '/vote', to: 'votes#create', as: :vote
  get '/:position/:number', to: 'candidates#show', as: :candidates
  get '/estatisticas(/:state/:position)', to: 'stats#charts', as: :charts
  get '/politica-de-privacidade', to: 'elections#politics', as: :politics
  get '/reportar-erro', to: 'issues#new', as: :issue

  root 'elections#index'

end
