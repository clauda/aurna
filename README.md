## A Urna

* Ruby 2.1.3
* Rails 4.1.6
* PostgreSQL

### Production

#### Unicorn

 `bundle exec unicorn -c config/unicorn.rb -D` starts unicorn.

#### Tasks

  `rake load:candidates[state]` Load candidates per state or all;


  Run Sidekiq `bundle exec sidekiq -v` to:


  `rake jobs:votes` Update candidates votes;

  `rake jobs:totals` Update candidates totals for stats;

  `rake jobs:stats` Update stats;
