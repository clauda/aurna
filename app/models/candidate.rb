class Candidate < ActiveRecord::Base
  include StatsHelper
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  validates :name, :ballot_name, :position, :number, :entourage, :status, :union, :election, presence: true
  validates :number, uniqueness: { scope: [:position, :state, :election_id] }

  has_many :votes
  has_many :stats
  belongs_to :election

  scope :active, -> { where(status: 'Deferido') }

  def self.decrement_vote id
    candidate = self.find id
    candidate.votes_counter ||= 0
    candidate.update_column :votes_counter, candidate.votes_counter-1 if candidate
  end

  def votes_to_percent
    # totals = TotalStat.where(election: self.election, position: self.position, state: self.state).first
    total = Candidate.active.where(election_id: self.election_id, position: self.position, state: self.state).sum(:votes_counter)
    self.votes_counter = 0 if self.votes_counter.nil?
    percent = (total > 0) ? (self.votes_counter * 100)/total : 0
    return percent
  end

  def votes_for_days count = 5
    days = last_days(count)
    stats = self.stats.betweens_dates(days.first, days.last)
    votes = Array.new(count - stats.size, 0)

    stats.order(:created_at).each do |stats|
      votes.push(stats.votes || 0)
    end

    return votes
  end

end
