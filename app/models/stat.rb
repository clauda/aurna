class Stat < ActiveRecord::Base
  belongs_to :candidate
  belongs_to :election

  validates :election, :candidate, presence: true
  validates :candidate_id, uniqueness: { scope: :created_at }

  scope :by_candidate, ->(candidate_id) { where(candidate_id: candidate_id) }
  scope :betweens_dates, ->(starts_at, ends_at) { where('stats.created_at BETWEEN ? AND ?', starts_at.beginning_of_day, ends_at.end_of_day) }
  scope :at, ->(date) { where('stats.created_at BETWEEN ? AND ?', date.beginning_of_day, date.end_of_day) }
end
