class Issue < ActiveRecord::Base

  STATUSES = {
    'Novo' => 'Novo',
    'Em andamento' => 'Em andamento',
    'Ignorado' => 'Ignorado',
    'Resolvido' => 'Resolvido'
  }

  belongs_to :user
  validates :message, presence: true

  before_create :init

  def init
    self.status = 'Novo'
  end
end
