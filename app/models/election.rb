# encoding: utf-8
class Election < ActiveRecord::Base
  validates :year, :round, presence: true, numericality: true
  validates :year, length: { maximum: 4 }
  validates :round, length: { in: 1..2 }
  validates :round, uniqueness: { scope: :year }

  has_many :votes
  has_many :candidates

  def name
    "Eleições #{self.year} - #{self.round}o Turno"
  end
end
