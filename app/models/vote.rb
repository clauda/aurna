class Vote < ActiveRecord::Base
  belongs_to :candidate
  belongs_to :election
  belongs_to :user

  validates :election, presence: true
  validates :user, presence: true

  validates :position, uniqueness: { scope: [:election_id, :user_id] }

  delegate :number, to: :candidate

  before_save :counter, if: :candidate_id_changed?

  def counter
    old_id = self.changes[:candidate_id][0] || nil
    Candidate.decrement_vote(old_id) if old_id

    if self.candidate
      self.candidate.update_column :votes_counter, (self.candidate.votes_counter||0)+1
    end
  end

end
