class VotesWorker
  include Sidekiq::Worker

  def perform candidate_id
    @candidate = Candidate.find candidate_id
    @candidate.update_column :votes_counter, @candidate.votes.size if @candidate
  end
end