class StatsWorker
  include Sidekiq::Worker

  def perform candidate_id
    @candidate = Candidate.find candidate_id

    Stat.create(candidate: @candidate, election: @candidate.election, votes: @candidate.votes_counter, percent: @candidate.votes_to_percent)
  end

end
