json.labels labels_for_days(5)

json.series @candidates.where('votes_counter > 0').each do |candidate|
  json.name candidate.ballot_name
  json.data candidate.votes_for_days(5)
end
