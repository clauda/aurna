$.ajaxSetup
  cache: true

class AUrna.Router extends Backbone.Router

  routes:
    'simulador(/)' : 'simulator'
    'estatisticas(/)' : 'charts'
    'estatisticas/:state/:position(/)' : 'charts'

  initialize: ->
    Backbone.history.start({ pushState: true })

  simulator: ->
    @simulator = new AUrna.SimulatorView()

  charts: (state, position)->
    @charts = new AUrna.ChartsView({ state: state, position: position })

