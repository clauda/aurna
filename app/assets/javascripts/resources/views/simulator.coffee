class AUrna.SimulatorView extends Backbone.View
  el: '#simulator'
  keys: [48, 49, 50, 51, 52, 53, 54, 55, 56, 57]

  events:
    'keypress input.input-square': 'nextField'
    'keyup input.input-square': 'resetInput'
    'click .correct': 'correct'
    'click .white': 'white'
    'submit form': 'submitBallot'

  initialize: ->
    @_isWhite = false
    @_wizard()
    $("input.input-square").first().focus()
    return

  resetInput: (e)->
    if !_.contains(@keys, e.keyCode)
      $(e.currentTarget).val("")
      $(e.currentTarget).focus()
    else
      @_lookup(e) if @handleNumber(e.target.form.id)
      if $(e.currentTarget).hasClass("last")
        $("section.current .confirm").focus()

  nextField: (e)->
    if _.contains(@keys, e.charCode)
      $(e.currentTarget).next("input.input-square").focus()
      return

  correct: (e)->
    e.preventDefault()
    currentForm = e.target.form.id

    $("##{currentForm} .confirm").prop(disabled: true).addClass('btn-disabled')
    $("##{currentForm}").find('.guy').html('')
    _.each $("##{currentForm} input.input-square"), (el)-> $(el).val('').prop('disabled', false)
    $("##{currentForm} input.input-square").first().focus()
    return

  white: (e)->
    @_isWhite = true
    @correct(e)
    _.each $("##{e.target.form.id} input.input-square"), (el)-> $(el).prop('disabled', true)
    $("##{e.target.form.id}")
      .find('.guy')
      .html('<span style="color:white;background:gray;padding:5px 40px 35px">VOTO EM BRANCO</span><p style="color:silver;font-size:14px">Clique no botão laranja para <br>corrigir ou no verde para confirmar ;)</p>')
    @_allowSubmit(e.target.form.id)
    return

  submitBallot: (e)->
    e.preventDefault()
    @vote = new AUrna.VoteView()

    if @candidate && !@_isWhite
      params = {
        candidate_id: @candidate.model.id
        position: @candidate.model.attributes.position
        is_null: (!@candidate.model.id || false)
        is_white: @_isWhite
      }
    else
      params = {
        position: @_positions[e.target.id]
        is_null: false
        is_white: @_isWhite
      }

    @vote.place(params)

    if e.target.id == 'presidente'
      window.location = '/simulador/resultados'
    else
      $("a[href='#next']").trigger('click')
      $("section.current input.input-square").first().focus()
    return

  handleNumber: (form)->
    ballot = ''
    _.each $("##{form} input.input-square"), (el)-> ballot += $(el).val()
    return ballot

  _lookup: (e)->
    el = e.target.form.id
    ballot = @handleNumber(el)
    if ballot.length == @_inputSizes[el]
      @candidate = new AUrna.CandidateView({ position: @_positions[el], ballot: ballot }, el)
      @_isWhite = false
      @_allowSubmit(el)
    return

  _allowSubmit: (form)->
    $("##{form} .confirm").prop(disabled: false).removeClass('btn-disabled')

  _wizard: ->
    @urna = @$el.steps
      headerTag: 'h3'
      bodyTag: 'section'
      transitionEffect: 'slideLeft'
      autoFocus: true
      enablePagination: true
      enableAllSteps: false
      enableFinishButton: true
      labels:
        current: ''
        finish: 'FIM'
        next: 'Próximo'
        previous: 'Anterior'
        loading: 'Aguarde um minuto...'

  _inputSizes:
    'deputado-estadual': 5
    'deputado-federal': 4
    'senador': 3
    'governador': 2
    'presidente': 2

  _positions:
    'deputado-estadual': 'Deputado Estadual'
    'deputado-federal': 'Deputado Federal'
    'senador': 'Senador'
    'governador': 'Governador'
    'presidente': 'Presidente'

