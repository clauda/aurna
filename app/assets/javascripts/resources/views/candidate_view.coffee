class AUrna.CandidateView extends Backbone.View
  template: _.template("<%- ballot_name %> (<%- entourage %>)")

  initialize: (params, route)->
    @route = route
    @model = new AUrna.CandidateModel({ number: params.ballot, position: params.position })

    _self = @
    defer = $.Deferred()

    $.when(@lookup())
      .done ->
        _self.render()

    return

  render: ->
    $("##{@route} .guy").html(@template(@model.attributes))
    @_animate('shake')
    @

  lookup: ->
    defer = $.Deferred()

    @model.fetch
      url: "/#{@route}/#{@model.attributes.number}.json"
      success: ((model, response, options)->
        defer.resolveWith @, model
      ).memoize()

    defer.promise()

  _animate: (effect)->
    $("##{@route} .guy")
      .addClass("#{effect} animated")
      .one 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', ->
        $(this).removeClass("#{effect} animated")

