class AUrna.VoteView extends Backbone.View

  initialize: (params)->
    @model = new AUrna.VoteModel()
    return

  place: (params)->
    defer = $.Deferred()

    @model.save
      candidate_id: params.candidate_id
      position: params.position
      is_null: params.is_null || false
      is_white: params.is_white || false
      success: ((model, response, options)->
        defer.resolveWith @, model
      ).memoize()

    defer.promise()

