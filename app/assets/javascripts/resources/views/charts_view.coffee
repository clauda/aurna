class AUrna.ChartsView extends Backbone.View
  el: '.ct-chart'

  events:
    'mouseenter .ct-point': '_hover'
    'mouseleave .ct-point': '_leave'
    'mousemove': '_move'

  options:
    showPoint: true
    lineSmooth: false
    seriesBarDistance: 10
    axisX:
      showGrid: false
      showLabel: true
      scaleMinSpace: 10000
    axisY:
      offset: 15

  initialize: (params)->
    @state = params.state || 'BR'
    @position = params.position || 'presidente'

    @model = new AUrna.StatModel({ state: @state, position: @position })
    @lookup()

    document.title = "Intenções de votos para #{@_capitalize(@position)} do #{@_acronym()} - Eleições 2014 - A Urna"
    @$toolTip = @$el.append('<div class="tip"></div>').find('.tip').hide()
    @

  lookup: ->
    _self = @
    defer = $.Deferred()

    @model.fetch
      url: "/estatisticas/#{@state}/#{@position}.json"
      success: ((model, response, options)->
        Chartist.Line('.ct-chart', { labels: response.labels, series: response.series }, _self.options)
        defer.resolveWith @, model
      ).memoize()

    defer.promise()

  _hover: (e)->
    _self = @
    $point = $(e.currentTarget)
    value = $point.attr 'ct:value'
    seriesName = $point.parent().attr('ct:series-name')

    $point.animate {'stroke-width': '50px'}, 300, @_easeOutQuad
    _self.$toolTip.html(seriesName + '<br>' + value).show()

  _leave: (e)->
    $point = $(e.currentTarget)

    $point.animate {'stroke-width': '20px'}, 300, @_easeOutQuad
    @$toolTip.hide()

  _move: ->
    @$toolTip.css
      left: event.offsetX - @$toolTip.width() / 2 - 10,
      top: event.offsetY - @$toolTip.height() - 40

  _easeOutQuad: (x, t, b, c, d)->
    return -c * (t /= d) * (t - 2) + b

  _capitalize: (string)->
    string.replace /^[a-z]/, (m)-> return m.toUpperCase()

  _acronym: ->
    if @state == 'BR'
      return 'Brasil'
    else
      @state
