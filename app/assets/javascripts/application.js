// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require json2
//= require underscore-min
//= require backbone-min
//= require vendor/hover_pack
//= require zone/bootstrap.min
//= require vendor/jquery.steps.min
//= require vendor/chartist.min
//= require memoizer

//= require aurna
//= require resources/router
//= require_tree ./resources/models
//= require_tree ./resources/views

$.ajaxSetup({ cache: true });
Backbone.emulateHTML = true;
Backbone.emulateJSON = false;
var router = new AUrna.Router();

$('.btn-fb').on('click', function(){
  FB.ui({
    method: 'share_open_graph',
    action_type: 'og.likes',
    action_properties: JSON.stringify({
      object:'http://www.aurna.com.br',
    })
  }, function(response){});
  return false;
});
