module StatsHelper

  def labels_for_days count = 5
    days = last_days(count)
    days.map.with_index { |day, i| days[i] = day.strftime('%d/%m') }
    return days
  end

  protected

    def last_days count
      days = []
      count.times.reverse_each { |i| days.push(Date.yesterday - i.day) }
      return days
    end

end
