module Zone::IssuesHelper

  STATUSES = {
    'novo' => 'success',
    'em andamento' => 'warning',
    'ignorado' => 'mute',
    'resolvido' => 'none'
  }

  def class_for status
    return STATUSES[status.downcase]
  end

end
