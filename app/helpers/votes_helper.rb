module VotesHelper

  def glue_for votes
    html = ""
    order_positions.each do |position|
      vote = votes.where(position: position).first
      html << "<div class='#{position.parameterize}'><h4>#{position}</h4>#{presenter_for vote}</div>" if vote
    end
    return raw(html)
  end


  protected

    def order_positions
      CandidatesHelper::POSITIONS.values.to_a.push 'Deputado Distrital'
    end

    def presenter_for vote
      if vote.is_white?
        raw("<h4>VOTO EM BRANCO</h4>")
      elsif vote.is_null?
        raw("<h4>VOTO NULO</h4>")
      elsif vote.candidate
        html = ""
        vote.candidate.try(:number).to_s.each_char do |number|
          html << "<span class='square'>#{number}</span>"
        end
        html << "<br><br>"
        raw(html)
      else
        raw("<h4>VOTO EM BRANCO</h4>")
      end
    end

end
