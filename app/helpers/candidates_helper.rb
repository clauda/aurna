module CandidatesHelper

  POSITIONS = {
    'deputado-estadual' => 'Deputado Estadual',
    'deputado-federal' => 'Deputado Federal',
    'governador' => 'Governador',
    'senador' => 'Senador',
    'presidente' => 'Presidente'
   }

  STATES = {
    'AC' => 'Acre',
    'AL' => 'Alagoas',
    'AM' => 'Amazonas',
    'AP' => 'Amapá',
    'BA' => 'Bahia',
    'CE' => 'Ceará',
    'DF' => 'Distrito Federal',
    'ES' => 'Espírito Santo',
    'GO' => 'Goiás',
    'MA' => 'Maranhão',
    'MG' => 'Minas Gerais',
    'MS' => 'Mato Grosso do Sul',
    'MT' => 'Mato Grosso',
    'PA' => 'Pará',
    'PB' => 'Paraíba',
    'PE' => 'Pernambuco',
    'PI' => 'Piauí',
    'PR' => 'Paraná',
    'RJ' => 'Rio de Janeiro',
    'RN' => 'Rio Grande do Norte',
    'RR' => 'Roraima',
    'RO' => 'Rondônia',
    'RS' => 'Rio Grande do Sul',
    'SC' => 'Santa Catarina',
    'SE' => 'Sergipe',
    'SP' => 'São Paulo',
    'TO' => 'Tocantins'
  }

  STATES_2ROUND = {
    'AC' => 'Acre',
    'AM' => 'Amazonas',
    'AP' => 'Amapá',
    'CE' => 'Ceará',
    'DF' => 'Distrito Federal',
    'GO' => 'Goiás',
    'MS' => 'Mato Grosso do Sul',
    'PA' => 'Pará',
    'PB' => 'Paraíba',
    'RJ' => 'Rio de Janeiro',
    'RN' => 'Rio Grande do Norte',
    'RR' => 'Roraima',
    'RO' => 'Rondônia',
    'RS' => 'Rio Grande do Sul'
  }

end
