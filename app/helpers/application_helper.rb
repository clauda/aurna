module ApplicationHelper

  # Add metatags on head.
  # Usage:
  #   <% seo [
  #     { property: 'description', content: nil },
  #     { property: 'keywords', content: nil }
  #   ] %>
  # on views.
  # description and keywords metatags has default values on settings.yml.
  def seo metatags
    metatags.map do |meta|
      metatag meta[:property], meta[:content], (meta[:type] || 'name')
    end
  end

  def metatag property, content = nil, type = 'name'
    default = content ? content : SETTINGS[:meta][property.to_sym]
    content_for :metatags do
      raw "<meta #{type}='#{property}' content='#{default}'>\n"
    end
  end

  def title page_title = nil
    content_for(:title) { page_title || "Eleições 2014 - Intenções de Voto em Tempo Real - A Urna" }
  end

end
