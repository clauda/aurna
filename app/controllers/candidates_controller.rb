class CandidatesController < InheritedResources::Base
  include CandidatesHelper

  before_action :authenticate_user!
  actions :show

  def show
    # TODO: elasticsearch aqui
    position = CandidatesHelper::POSITIONS[params[:position]]

    unless params[:position] == 'presidente'
      position = 'Deputado Distrital' if current_user.state == 'DF'
      @candidate = Candidate.active.where(position: position,
              election: current_election,
              number: params[:number],
              state: current_user.state).first
    else
      @candidate = Candidate.active.where(position: position,
              election: current_election,
              number: params[:number],
              state: 'BR').first
    end

    render json: (@candidate || { ballot_name: 'Vote Nulo', entourage: 'Vote Nulo' })
  end

end
