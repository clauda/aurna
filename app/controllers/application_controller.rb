class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?

  helper_method :current_election
  helper_method :current_election_id

  def authenticate_admin!
    authenticate_user!
    redirect_to root_path unless current_user.admin?
  end

  def after_sign_in_path_for resource
    return simulator_path
  end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up){ |u| u.permit(:name, :state, :email, :password, :password_confirmation) }
    end

    def current_election
      @current_election ||= Election.where(year: SETTINGS[:election][:year], round: SETTINGS[:election][:round]).first
    end

    def current_election_id
      @current_election_id ||= current_election.id
    end
end
