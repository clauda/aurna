class UsersController < InheritedResources::Base
  actions :show, :edit, :update, :destroy
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def account
    @user = current_user
    # authorize! :update, @user
    if request.patch? && params[:user] #&& params[:user][:email]
      if @user.update(user_params)
        sign_in(@user, bypass: true)
        redirect_to account_path, notice: 'Dados salvos!'
      end
    end
  end

  private

    def user_params
      accessible = [ :email, :name, :state ]
      accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
      params.require(:user).permit(accessible)
    end
end