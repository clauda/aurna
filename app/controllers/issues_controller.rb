class IssuesController < ApplicationController

  def new
    @issue = Issue.new
  end

  def create
    @issue = Issue.new issue_params
    @issue.user = current_user if current_user
    @issue.save
    redirect_to :back, notice: 'Valeu! Recebemos seu recado!'
  end

  private

    def issue_params
      params.require(:issue).permit(:message)
    end
end
