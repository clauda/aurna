class Zone::IssuesController < InheritedResources::Base
  before_action :authenticate_admin!
  layout 'admin'
  actions :index, :show, :edit, :update

  private

    def issue_params
      params.require(:issue).permit(:status)
    end

end
