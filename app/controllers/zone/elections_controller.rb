class Zone::ElectionsController < InheritedResources::Base
  before_action :authenticate_admin!
  layout 'admin'

  def index
    @election = Election.new
    super
  end

  def create
    super do |format|
      format.html { redirect_to zone_elections_url }
    end
  end

  private

    def election_params
      params.require(:election).permit(:year, :round)
    end

end
