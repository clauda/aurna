class Zone::DashboardController < ApplicationController
  before_action :authenticate_admin!
  layout 'admin'

  def index
    @votes = Vote.where(election_id: current_election.id).size
    @users = User.all.size
    @candidates = Candidate.active.where(election_id: current_election.id).size
    @issues = Issue.all.size
  end

  def stats
    @candidates =
      Candidate.active.where(election_id: current_election.id,
        position: (params[:position].try(:capitalize)||'Presidente'),
        state: (params[:state].try(:upcase)||'BR')).order(:id)
    @data = @candidates.select(:ballot_name, :votes_counter).to_json
  end

end
