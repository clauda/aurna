class Zone::CandidatesController < InheritedResources::Base
  include CandidatesHelper
  before_action :authenticate_admin!
  layout 'admin'

  def index
    @candidates = params[:term] ? Candidate.search(params[:term]).results : Candidate.active.where(election: current_election).page(params[:page])
    render action: :index
  end

  private

    def candidate_params
      params.require(:candidate).permit(:name, :ballot_name, :position, :number, :entourage, :status, :union, :state, :election_id)
    end
end
