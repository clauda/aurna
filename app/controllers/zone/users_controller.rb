class Zone::UsersController < InheritedResources::Base
  before_action :authenticate_admin!
  layout 'admin'
  actions :index, :edit, :update

  def index
    @users = User.all.order(id: :desc).page params[:page]
  end

  protected

    def user_params
      params.require(:user).permit(:email, :name, :state)
    end
end
