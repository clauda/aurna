# encoding: utf-8
class StatsController < ApplicationController
  respond_to :html, :json

  def charts
    @candidates = Candidate.active.where(
      election: current_election,
      position: (params[:position].try(:capitalize)||'Presidente'),
      state: (params[:state].try(:upcase)||'BR')).order(:id)
  end

end
