# encoding: utf-8
class ElectionsController < ApplicationController

  def index
    @candidates = Candidate.active.where(election: current_election)
    @votes = Vote.where(election: current_election)
    @users = User.all
    @dilma = @candidates.find_by(number: 13, position: 'Presidente')
    @aecio = @candidates.find_by(number: 45, position: 'Presidente')

    @stats = @candidates.where(position: 'Presidente').pluck(:ballot_name, :votes_counter).to_h
  end

  def politics; end

end
