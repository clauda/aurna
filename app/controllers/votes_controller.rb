class VotesController < InheritedResources::Base
  before_action :authenticate_user!

  def create
    @vote = current_user.votes.find_or_create_by(position: params[:position], election_id: current_election_id)
    @vote.update candidate_id: params[:candidate_id], is_null: params[:is_null], is_white: (params[:is_white] || false)
    render nothing: true
  end

  def results
    @votes = current_user.votes.where(election_id: current_election_id)
  end

  def print
    @votes = current_user.votes.where(election_id: current_election_id)
    render layout: 'print'
  end

end
